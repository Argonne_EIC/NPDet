NPDet - Nuclear Physics Detector Library
========================================

Overview
--------

A detector library for simulation and reconstruction based on 
[DD4hep](https://github.com/AIDAsoft/DD4hep). This library makes  full detector 
simulation and reconstruction easier by providing a set of flexible 
**parameterized detectors**. From these detectors, a full blown 
concept-detector can be created.

Installation
------------

### Dependencies

The following are needed before building `NPDet`

* [DD4hep](https://github.com/AIDAsoft/DD4hep)
* [ROOT](https://root.cern.ch)
* [GEANT4](http://geant4.cern.ch)


### Building

```
git clone https://whit@eicweb.phy.anl.gov/EIC/NPDet.git
mkdir npdet_build && cd npdet_build
cmake ../NPDet/. -DCMAKE_INSTALL_PREFIX=$HOME # or whereever
make -j4
make install
```


Libraries
---------

### Generic Detectors

[more details about Generic Detectors](src/GenericDetectors/README.md)

### Concept-Detectors

#### SiEIC

[more details about SiEIC](src/SiEIC/README.md)

#### JLEIC

[more details about JLEIC](src/JLEIC/README.md)

#### SoLID

[more details about JLEIC](src/SoLID/README.md)

#### clas12

[more details about JLEIC](src/clas12/README.md)

#### eRHIC

[more details about eRHIC](src/eRHIC/README.md)


Related Projects and Useful Links
---------------------------------

- [nprec](https://eicweb.phy.anl.gov/EIC/nprec)
- [lcgeo](https://github.com/iLCSoft/lcgeo)
- [FCCSW](https://github.com/HEP-FCC/FCCSW), [fcc](http://fccsw.web.cern.ch/fccsw/)
- [PODIO](https://github.com/HEP-FCC/podio)
- [HepMC3](https://gitlab.cern.ch/hepmc/HepMC3.git)

