eRHIC
======

![SoLID detector](compact/eRHIC.png)

Just the magnet for now.

Using `compact/run_example` :

```bash
cd compact
./run_example
```

This runs geant4 using `ddsim` with the general particle source configured in 
`compact/gps.mac`. The compact file `eRHIC.xml` is used  as the geometry.
The script must be executed from the `compact` directory.


