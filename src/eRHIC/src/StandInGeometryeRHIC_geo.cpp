#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "TMath.h"

using namespace std;
using namespace DD4hep;
using namespace DD4hep::Geometry;


static Ref_t create_detector(LCDD& lcdd, xml_h e, SensitiveDetector sens)
{
  xml_det_t  x_det     = e;
  string     det_name  = x_det.nameStr();
  Material   air       = lcdd.air();
  DetElement sdet        (det_name,x_det.id());
  Assembly   assembly    ("SOLID_standin_assembly");

  PlacedVolume pv;

  int n = 0;
  using namespace dd4hep;

  // ---------------------------------------------
  // Solenoid coil
  std::vector<double> rInner = {
    1220.0*mm, 1220.0*mm
  };
  std::vector<double> rOuter = { 
    1320.0*mm, 1320.0*mm
  };
  std::vector<double> zPlane = { 
    -1200.0*mm/2.0, 1200.0*mm/2.0  
  };

  Polycone solenoid( 0,  ///< Initial Phi starting angle
      360*deg,  ///< Total Phi angle
      rInner,        ///< Tangent distance to inner surface
      rOuter,        ///< Tangent distance to outer surface
      zPlane);       ///< z coordinate of corners
  Position solenoid_pos = {0*cm,0.0*cm,0.0*cm};
  Volume solenoid_vol("solenoid_vol", solenoid, air);
  solenoid_vol.setVisAttributes(lcdd, "GrayVis");
  pv = assembly.placeVolume(solenoid_vol, solenoid_pos);

  // ---------------------------------------------
  // solenoid2 coil
  rInner = {
    1200.0*mm, 1200.0*mm
  };
  rOuter = { 
    1340.0*mm, 1340.0*mm
  };
  zPlane = { 
    -1200.0*mm/2.0-638.5*mm, -1200.0*mm/2.0  
  };

  Polycone solenoid2( 0,  ///< Initial Phi starting angle
      360*deg,  ///< Total Phi angle
      rInner,        ///< Tangent distance to inner surface
      rOuter,        ///< Tangent distance to outer surface
      zPlane);       ///< z coordinate of corners
  Position solenoid2_pos = {0*cm,0.0*cm,0.0*cm};
  Volume solenoid2_vol("solenoid2_vol", solenoid2, air);
  solenoid2_vol.setVisAttributes(lcdd, "BlueVis");
  pv = assembly.placeVolume(solenoid2_vol, solenoid2_pos);


  // ---------------------------------------------
  // solenoid3 coil
  rInner = {
    1200.0*mm, 1200.0*mm
  };
  rOuter = { 
    1340.0*mm, 1340.0*mm
  };
  zPlane = { 
    1200.0*mm/2.0, 1200.0*mm/2.0 + 638.5*mm 
  };

  Polycone solenoid3( 0,  ///< Initial Phi starting angle
      360*deg,  ///< Total Phi angle
      rInner,        ///< Tangent distance to inner surface
      rOuter,        ///< Tangent distance to outer surface
      zPlane);       ///< z coordinate of corners
  Position solenoid3_pos = {0*cm,0.0*cm,0.0*cm};
  Volume solenoid3_vol("solenoid3_vol", solenoid3, air);
  solenoid3_vol.setVisAttributes(lcdd, "BlueVis");
  pv = assembly.placeVolume(solenoid3_vol, solenoid3_pos);

  // ---------------------------------------------
  // TPC coil
  rInner = {
    190.0*mm, 190.0*mm
  };
  rOuter = { 
    700.0*mm, 700.0*mm
  };
  zPlane = { 
    -1000.0*mm, 1000.0*mm
  };

  Polycone TPC( 0,  ///< Initial Phi starting angle
      360*deg,  ///< Total Phi angle
      rInner,        ///< Tangent distance to inner surface
      rOuter,        ///< Tangent distance to outer surface
      zPlane);       ///< z coordinate of corners
  Position TPC_pos = {0*cm,0.0*cm,0.0*cm};
  Volume TPC_vol("TPC_vol", TPC, air);
  TPC_vol.setVisAttributes(lcdd, "OrangeVis");
  pv = assembly.placeVolume(TPC_vol, TPC_pos);
  //______________________________________________________________________________
  //______________________________________________________________________________

  pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
  pv.addPhysVolID("system",sdet.id()).addPhysVolID("barrel",0);
  sdet.setPlacement(pv);
  return sdet;

}

DECLARE_DETELEMENT(StandInGeometryeRHIC,create_detector)
