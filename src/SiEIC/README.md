SiEIC
=====

![SiEIC detector](compact/sieic.png)

Note that no detector libraries are need because SiEIC uses those supplied by 
DD4hep.

Using `compact/run_example` :

```bash
cd compact
./run_example
```

This runs geant4 using `ddsim` with the general particle source configured in 
`compact/gps.mac`. The compact file `sieic.xml` is used  as the geometry.
The script must be executed from the `compact` directory.


