#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "TMath.h"
#include "DCGeometry.h"
#include "ALERTRecoilChamber.h"

using namespace std;
using namespace DD4hep;
using namespace DD4hep::Geometry;

static Ref_t create_detector(LCDD& lcdd, xml_h e, SensitiveDetector sens)
{
  xml_det_t  x_det     = e;
  string     det_name  = x_det.nameStr();
  Material   air       = lcdd.air();
  DetElement sdet        (det_name,x_det.id());
  Assembly   assembly    ("ALERTRecoilChamber_assembly");
  assembly.setVisAttributes( lcdd, x_det.visStr() );

  PlacedVolume pv;

  using namespace dd4hep;

  ALERTRecoilChamber alert;
  alert.BuildUnitCells();
  alert.BuildDetector(assembly);

  //---------------------------------
  double fScintWrapThickness  =  2.54*0.004*dd4hep::cm; 
  double fScintGap            =  2.54*0.002*dd4hep::cm; 

  double fScintLength         = 30.0*dd4hep::cm;
  double fScint2Length        = 3.0*dd4hep::cm;

  double fScint1Thickness     =  2.0*dd4hep::mm;
  double fScint2Thickness     = 20.0*dd4hep::mm;

  double fPhotonDetThickness =  25.4*0.00002*dd4hep::mm;

  double fInnerRadius        = 8.0*dd4hep::cm;
  double fScintDeltaTheta    = 6.0*dd4hep::degree; // angle subtended by bar 

  const int NScint = 60;

   // -----------------------------------------------------------
  double dy1 = fInnerRadius*tan(fScintDeltaTheta/2.0) - fScintWrapThickness;
  double dy2 = dy1 + fScint1Thickness*tan(fScintDeltaTheta/2.0);

  Trapezoid fScint1_solid(
      fScintLength/2.0, fScintLength/2.0,
      dy1             , dy2,
      fScint1Thickness/2.0 - fScintWrapThickness
      );

   // -----------------------------------------------------------
   double dyy1 = (fInnerRadius + fScint1Thickness )*tan(fScintDeltaTheta/2.0) - fScintWrapThickness;
   double dyy2 = dyy1 + fScint2Thickness*tan(fScintDeltaTheta/2.0);

   Trapezoid fScint2_solid(
         fScint2Length/2.0-fScintWrapThickness,fScint2Length/2.0-fScintWrapThickness,// half-length along x at -dz and +dz
         dyy1, dyy2,                                // half-length along y at -dz and +dz
         fScint2Thickness/2.0 - fScintWrapThickness);

   ROOT::Math::Rotation3D     scint_rot( RotationY(1.0*CLHEP::pi/2.0));
   ROOT::Math::XYZVector s1_pos = {fScint1Thickness/2.0 + fInnerRadius,0.0,0.0};
   ROOT::Math::XYZVector s2_pos = {fScint2Thickness/2.0 + fScint1Thickness + fInnerRadius,0.0,0.0};
   ROOT::Math::XYZVector fScint1_pos = {0.0,0.0,0.0};
   ROOT::Math::XYZVector fScint2_pos = {0.0,0.0,0.0};

   // -----------------------------------------------------------
   double delta_phi = 360.0*dd4hep::degree/double(NScint);
   std::string name       = "";

   for(int i = 0; i<60; i++) {

      s1_pos = RotationZ(delta_phi)*s1_pos;
      s2_pos = RotationZ(delta_phi)*s2_pos;
      scint_rot = RotationZ(delta_phi)*scint_rot;

      //fScint1_positions[i] = fScint1_pos + s1_pos;
      auto fScint1_positions = fScint1_pos + s1_pos;

      
      name                 = "fScint1_physicals_" + std::to_string(i);
      //fScint1_physicals[i] = new G4PVPlacement(
      //      G4Transform3D(scint_rot,fScint1_positions[i]), 
      //      fScint1_log, name, fContainer_log, false, i, checkOverlaps); 
      //name = "fScint1_borders_" + std::to_string(i);
      //fScint1_borders[i]  = new G4LogicalBorderSurface(name, fScint1_physicals[i], phys, OpSurface );

      Volume scint1_vol(name, fScint1_solid, air);
      scint1_vol.setVisAttributes(lcdd, "BlueVis");
      pv = assembly.placeVolume(scint1_vol,ROOT::Math::Transform3D(scint_rot, fScint1_positions));

      for(int j = 0; j<10; j++) {
         name                 = "fScint2_physicals_" + std::to_string(i) + "_" + std::to_string(j);
         auto fScint2_positions = fScint2_pos + s2_pos;
         ROOT::Math::XYZVector Z_offset = {0,0,(30.0*cm/10.0)*(double(j-5)+0.5)};
         fScint2_positions += Z_offset;

         Volume scint2_vol(name, fScint2_solid, air);
         scint2_vol.setVisAttributes(lcdd, "OrangeVis");
         pv = assembly.placeVolume(scint2_vol, ROOT::Math::Transform3D(scint_rot,fScint2_positions));

         //fScint2_physicals[j][i] = new G4PVPlacement(
         //      G4Transform3D(scint_rot,fScint2_positions[j][i]), 
         //      fScint2_log, name, fContainer_log, false, i*10+j, checkOverlaps); 
         //name = "fScint2_borders_" + std::to_string(i);
         //fScint2_borders[j][i]  = new G4LogicalBorderSurface(name, fScint2_physicals[j][i], phys, OpSurface );
      }
   }
  //______________________________________________________________________________


  pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
  pv.addPhysVolID("system",sdet.id()).addPhysVolID("barrel",0);
  sdet.setPlacement(pv);
  return sdet;


}

DECLARE_DETELEMENT(ALERTRecoilChamber,create_detector)
