//==========================================================================
//
// Generic Tube Creator. Will not support sensitive segments.
//
// Control via .xml file as follows:
//      <lcdd> <detectors>
// #Must contain a detector child element,
//      <detector name ="DetName" type="GenericShape" >
// #Must define at least one layer,
//      <layer id="#(int)" inner_r="#(double)" outer_z="#(double)" >
// #Must define at least one slice,
//      <slice material="string" thickness="#(double)" >         
// #Close layer, detector,
//      </layer>
//      </detector>
//      </lcdd>
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "TMath.h"
#include "DCGeometry.h"

using namespace std;
using namespace DD4hep;
using namespace DD4hep::Geometry;

//namespace clas12 {
//      double y_center(int L) const { return( fa_params.at(1)*double(L-1) ); } 
//      double dy_center(int L) const { return( fa_params.at(2) + fa_params.at(3)*double(L-1) ); } 
//
//      double y_pos(int L, double x) const { return( y_center(L) - dy_center(L) + fTanTheta_0*x ); }
//      double y_neg(int L, double x) const { return( y_center(L) - dy_center(L) - fTanTheta_0*x ); }
//      double y_outer(int L) const { return( y_center(L) + dy_center(L) ); }
//      //______________________________________________________________
//
//      double Y_U(int L,int U,double x) const {
//         return( -fa_params.at(2) - fa_params.at(4)*double(L-1)  + w_U(L)*double(U-1));
//      } 
//      double Y_V(int L,int V,double x) const {
//         return( y_center(L) - dy_center(L) + w_V(L)*(36+1-V)*TMath::Sqrt( 1.0+fTanTheta_0*fTanTheta_0) + fTanTheta_0*x );
//      } 
//      double Y_W(int L,int W,double x) const {
//         return( y_center(L) - dy_center(L) + w_W(L)*(36+1-W)*TMath::Sqrt( 1.0+fTanTheta_0*fTanTheta_0) - fTanTheta_0*x );
//      } 
//
//      double w_U(int L) const { return( fa_params.at(5) + fa_params.at(6)*(L-1) ); } 
//      double w_V(int L) const { return( fa_params.at(7) + fa_params.at(8)*(L-2) ); } 
//      double w_W(int L) const { return( fa_params.at(9) + fa_params.at(8)*(L-3) ); } 
//      //______________________________________________________________
//
//      double d_1(int L) const {
//         double d2 = fd2_param ;
//         if( L > 15 ) d2 = fd2prime_param;
//         return( d2 + (w_V(L)/2.0)*((fTanTheta_0*fTanTheta_0 - 1.0)/(fTanTheta_0)) );
//      }
//
//      //______________________________________________________________
//      double x_corner_U1(int L, int U) const {
//         return( (y_center(L) - dy_center(L) + fa_params.at(2) + fa_params.at(4)*double(L-1) - double(U-1)*w_U(L))/fTanTheta_0 );
//      }
//      double x_corner_U2(int L, int U) const {
//         return( -1.0*x_corner_U1(L,U) );
//      }
//      double y_corner_U1(int L, int U) const {
//         return( y_neg(L, x_corner_U1(L,U) ) );
//      }
//      double y_corner_U2(int L, int U) const {
//         return( y_pos(L, x_corner_U2(L,U) ) );
//      }
//      //______________________________________________________________
//
//      double x_corner_V1(int L, int V) const {
//         return( (-1.0*y_outer(L) + y_center(L) - dy_center(L) + 
//                double(fNStrips+1-V)*w_V(L)*TMath::Sqrt(1.0+fTanTheta_0*fTheta_0))/fTanTheta_0 );
//      }
//      double x_corner_V2(int L, int V) const {
//         return( (double(fNStrips+1-V)*w_V(L)*TMath::Sqrt(1.0+fTanTheta_0*fTheta_0))/(2.0*fTanTheta_0) );
//      }
//      double y_corner_V1(int L, int V) const {
//         return( y_outer(L) );
//      }
//      double y_corner_V2(int L, int V) const {
//         return( y_pos(L,x_corner_V2(L,V)) );
//      }
//      //______________________________________________________________
//
//      double x_corner_W1(int L, int W) const {
//         return( (y_outer(L) - 1.0*y_center(L) + dy_center(L) + 
//                double(fNStrips+1-W)*w_W(L)*TMath::Sqrt(1.0+fTanTheta_0*fTheta_0))/fTanTheta_0 );
//      }
//      double x_corner_W2(int L, int W) const {
//         return( -1.0*(double(fNStrips+1-W)*w_W(L)*TMath::Sqrt(1.0+fTanTheta_0*fTheta_0))/(2.0*fTanTheta_0) );
//      }
//      double y_corner_W1(int L, int W) const {
//         return( y_outer(L) );
//      }
//      double y_corner_W2(int L, int W) const {
//         return( y_neg(L,x_corner_W2(L,W)) );
//      }
//      //______________________________________________________________
//}

static Ref_t create_detector(LCDD& lcdd, xml_h e, SensitiveDetector sens)
{
  xml_det_t  x_det     = e;
  string     det_name  = x_det.nameStr();
  Material   air       = lcdd.air();
  DetElement sdet        (det_name,x_det.id());
  Assembly   assembly    ("cryostat_assembly");

  PlacedVolume pv;

  int n = 0;
   using namespace dd4hep;


  std::cout << " TESTING ...\n";

  double fL1 = 721.723*dd4hep::cm; // nominal distance from target to P (point on face where line is normal)
  Position fL1_pos = { 0.0, fL1*TMath::Sin(25.0*dd4hep::degree), fL1*TMath::Cos(25.0*dd4hep::degree) };

  double fLPO = 95.088*dd4hep::cm; // distance from P to 
  Position fS_pos = { 0.0, -fLPO*TMath::Cos(25.0*dd4hep::degree), fLPO*TMath::Sin(25.0*dd4hep::degree) };

  int    fNStrips = 36;
  int    fNViewLayers = 13;

  double fScintThickness     = 1.0*dd4hep::cm;
  double fPbThickness        = 0.2381*dd4hep::cm;
  double fLayerHalfThickness = 6.19*dd4hep::mm;
  double fTheta_0            = 62.88*dd4hep::degree;
  double fTanTheta_0         = TMath::Tan(fTheta_0);

  std::vector<double>  fa_params = {
    0.0,  // dummy to keep the indicies the same (starts at 1) 
      0.0856*dd4hep::mm,
      1864.6*dd4hep::mm,
      4.45635*dd4hep::mm,
      4.3708*dd4hep::mm,
      103.66*dd4hep::mm,
      0.2476*dd4hep::mm,
      94.701*dd4hep::mm,
      0.2256*dd4hep::mm,
      94.926*dd4hep::mm
  };

  double fd2_param      = 36.4*dd4hep::mm;
  double fd2prime_param = 25.4*dd4hep::mm;

  //Box temp_box   new G4Box("DC_placement_box_solid",5*m,5*m,5*m);

      //  4 _________ 3
      //    \ front /
      //     \     /
      //      \   /
      //       \_/
      //      1   2
   //std::cout <<  (y_center(2)) << std::endl;
   //std::cout << -dy_center(1) << std::endl;
   //std::cout <<  fa_params.at(2) + fa_params.at(4)*double(1-1) << std::endl; 
   //std::cout << double(1-1)*w_U(1) << std::endl;
   //std::cout << w_U(1) << std::endl;

  //______________________________________________________________
  auto  y_center = [&](int L) { return( fa_params.at(1)*double(L-1) ); };
  auto  dy_center = [&](int L) { return( fa_params.at(2) + fa_params.at(3)*double(L-1) ); };
  auto  y_pos = [&](int L, double x) { return( y_center(L) - dy_center(L) + fTanTheta_0*x ); };
  auto  y_neg = [&](int L, double x) { return( y_center(L) - dy_center(L) - fTanTheta_0*x ); };
  auto  y_outer = [&](int L) { return( y_center(L) + dy_center(L) ); };
  //______________________________________________________________
  auto  w_U = [&](int L) { return( fa_params.at(5) + fa_params.at(6)*(L-1) ); } ;
  auto  w_V = [&](int L) { return( fa_params.at(7) + fa_params.at(8)*(L-2) ); } ;
  auto  w_W = [&](int L) { return( fa_params.at(9) + fa_params.at(8)*(L-3) ); } ;


  auto Y_U = [&](int L,int U,double x) { return( -fa_params.at(2) - fa_params.at(4)*double(L-1)  + w_U(L)*double(U-1)); } ;
  auto Y_V = [&](int L,int V,double x) { return( y_center(L) - dy_center(L) + w_V(L)*(36+1-V)*TMath::Sqrt( 1.0+fTanTheta_0*fTanTheta_0) + fTanTheta_0*x ); } ;
  auto Y_W = [&](int L,int W,double x) { return( y_center(L) - dy_center(L) + w_W(L)*(36+1-W)*TMath::Sqrt( 1.0+fTanTheta_0*fTanTheta_0) - fTanTheta_0*x ); } ;

  auto x_corner_U1 = [&](int L, int U) {
    return( (y_center(L) - dy_center(L) + fa_params.at(2) + fa_params.at(4)*double(L-1) - double(U-1)*w_U(L))/fTanTheta_0 );
  };

  auto x_corner_U2 = [&](int L, int U) {
    return( -1.0*x_corner_U1(L,U) );
  };
  auto y_corner_U1 = [&](int L, int U) {
    return( y_neg(L, x_corner_U1(L,U) ) );
  };
  auto y_corner_U2 = [&](int L, int U)  {
    return( y_pos(L, x_corner_U2(L,U) ) );
  };
 auto  x_corner_W1 = [&](int L, int W) {
    return( (y_outer(L) - 1.0*y_center(L) + dy_center(L) + 
           double(fNStrips+1-W)*w_W(L)*TMath::Sqrt(1.0+fTanTheta_0*fTheta_0))/fTanTheta_0 );
 };
 auto x_corner_W2 = [&](int L, int W) {
    return( -1.0*(double(fNStrips+1-W)*w_W(L)*TMath::Sqrt(1.0+fTanTheta_0*fTheta_0))/(2.0*fTanTheta_0) );
 };
 auto y_corner_W1 = [&](int L, int W) {
    return( y_outer(L) );
 };
 auto y_corner_W2 = [&](int L, int W) {
    return( y_neg(L,x_corner_W2(L,W)) );
 };
 //______________________________________________________________

   std::vector<double> fContainer_points = {
      -fd2_param/2.0 + x_corner_U1(39,1),   1.0*y_corner_U1(39, 1) ,
       fd2_param/2.0 + x_corner_U2(39,1),   1.0*y_corner_U1(39, 1) ,
       fd2_param/2.0 + x_corner_U2(39,36),  1.0*y_corner_U2(39,36) ,
      -fd2_param/2.0 + x_corner_U1(39,36),  1.0*y_corner_U2(39,36) ,
      -fd2_param/2.0 + x_corner_U1(39,1),  1.0*y_corner_U1(39, 1) ,
       fd2_param/2.0 + x_corner_U2(39,1),  1.0*y_corner_U1(39, 1) ,
       fd2_param/2.0 + x_corner_U2(39,36), 1.0*y_corner_U2(39,36) ,
      -fd2_param/2.0 + x_corner_U1(39,36), 1.0*y_corner_U2(39,36) 
   };
   //for(auto p: fContainer_points) {
   //   std::cout << " x = " << p.x()/cm << ", y = " << p.y()/cm << " cm \n";
   //}
   ////pv = assembly.placeVolume(s_vol,Position(0, 0, z_offset));


   // placment 
   for(int sec = 1;  sec <= 6; sec++) {
     //std::cout << "sec      : " << sec << std::endl;
     double angle = double(sec-1)*60.0*dd4hep::degree;
     ROOT::Math::RotationZ  rot2(-90.0*dd4hep::degree + angle);
     //ROOT::Math::RotationX rot_a(-25.0*dd4hep::degree);
     ROOT::Math::Rotation3D rot( ROOT::Math::RotationZ(-90.0*dd4hep::degree + angle)*ROOT::Math::RotationX(-25.0*dd4hep::degree) );
     //rot.rotateZ(-90.0*dd4hep::degree + angle);

     Transform3D tr( rot, rot2*(fL1_pos + fS_pos));
     EightPointSolid  fContainer_solid(39.0*fLayerHalfThickness, fContainer_points.data());
     Material mat = air;
     Volume EC_vol("EC_vol", fContainer_solid, mat);
     DetElement layer(sdet,std::string("EC_det_")+std::to_string(sec),sec);
     EC_vol.setVisAttributes(lcdd,x_det.visStr());
     pv = assembly.placeVolume( EC_vol, tr );
   }

   // ---------------------------------------------------------
   // DC
   // ---------------------------------------------------------
   //
   Box temp_box(5*m,5*m,5*m);
   // Region 1
   auto R1_points = clas12::geo::ContainerTrapPoints(1);
   std::vector<double> R1_points2;
   std::for_each(R1_points.begin(), R1_points.end(), [&](const auto& twovec){
       R1_points2.push_back( twovec.x()*dd4hep::mm );
       R1_points2.push_back( twovec.y()*dd4hep::mm );
       });

   EightPointSolid   R1_sol1( clas12::geo::ContainerTrapWidth(1)*dd4hep::mm, R1_points2.data() );
   IntersectionSolid R1_sol2( temp_box, R1_sol1,  Position(0.0,0.0, clas12::geo::ContainerTrapWidth(1)*dd4hep::mm) );

   auto R1_trap_points = clas12::geo::RegionTrapPoints(1);
   std::vector<double> R1_trap_points2;
   std::for_each(R1_trap_points.begin(), R1_trap_points.end(), [&](const auto& twovec){
       R1_trap_points2.push_back( twovec.x()*dd4hep::mm );
       R1_trap_points2.push_back( twovec.y()*dd4hep::mm );
       });

   EightPointSolid   R1_trap_sol1( clas12::geo::RegionTrapWidth(1)*dd4hep::mm, R1_trap_points2.data() );
   IntersectionSolid R1_trap_sol2( temp_box, R1_trap_sol1,  Position(0.0,0.0, clas12::geo::RegionTrapOffset(1)*dd4hep::mm) );

   // Region 2
   auto R2_points = clas12::geo::ContainerTrapPoints(2);
   std::vector<double> R2_points2;
   std::for_each(R2_points.begin(), R2_points.end(), [&](const auto& twovec){
       R2_points2.push_back( twovec.x()*dd4hep::mm );
       R2_points2.push_back( twovec.y()*dd4hep::mm );
       });

   EightPointSolid   R2_sol1( clas12::geo::ContainerTrapWidth(2)*dd4hep::mm, R2_points2.data() );
   IntersectionSolid R2_sol2( temp_box, R2_sol1,  Position(0.0,0.0, clas12::geo::ContainerTrapWidth(2)*dd4hep::mm) );

   auto R2_trap_points = clas12::geo::RegionTrapPoints(2);
   std::vector<double> R2_trap_points2;
   std::for_each(R2_trap_points.begin(), R2_trap_points.end(), [&](const auto& twovec){
       R2_trap_points2.push_back( twovec.x()*dd4hep::mm );
       R2_trap_points2.push_back( twovec.y()*dd4hep::mm );
       });

   EightPointSolid   R2_trap_sol1( clas12::geo::RegionTrapWidth(2)*dd4hep::mm, R2_trap_points2.data() );
   IntersectionSolid R2_trap_sol2( temp_box, R2_trap_sol1,  Position(0.0,0.0, clas12::geo::RegionTrapOffset(2)*dd4hep::mm) );

   // Region 3
   auto R3_points = clas12::geo::ContainerTrapPoints(3);
   std::vector<double> R3_points2;
   std::for_each(R3_points.begin(), R3_points.end(), [&](const auto& twovec){
       R3_points2.push_back( twovec.x()*dd4hep::mm );
       R3_points2.push_back( twovec.y()*dd4hep::mm );
       });

   EightPointSolid   R3_sol1( clas12::geo::ContainerTrapWidth(3)*dd4hep::mm, R3_points2.data() );
   IntersectionSolid R3_sol2( temp_box, R3_sol1,  Position(0.0,0.0, clas12::geo::ContainerTrapWidth(3)*dd4hep::mm) );

   auto R3_trap_points = clas12::geo::RegionTrapPoints(3);
   std::vector<double> R3_trap_points2;
   std::for_each(R3_trap_points.begin(), R3_trap_points.end(), [&](const auto& twovec){
       R3_trap_points2.push_back( twovec.x()*dd4hep::mm );
       R3_trap_points2.push_back( twovec.y()*dd4hep::mm );
       });

   EightPointSolid   R3_trap_sol1( clas12::geo::RegionTrapWidth(3)*dd4hep::mm, R3_trap_points2.data() );
   IntersectionSolid R3_trap_sol2( temp_box, R3_trap_sol1,  Position(0.0,0.0, clas12::geo::RegionTrapOffset(3)*dd4hep::mm) );

   // ----------------------------------------------------
   // Region placements
   //for(int i = 0;i<3;i++){
   
   
   int region = 1;
   Position width_diff = {
     0.0,
     1.0*clas12::geo::ContainerExtraShift(region)*dd4hep::mm,
     (clas12::geo::ContainerTrapWidth(region)*dd4hep::mm-clas12::geo::RegionTrapWidth(region)*dd4hep::mm)
   };

   Volume     DC_container_R1_vol("DC_container_R1_vol", R1_sol2, air);
   DetElement DC_container_R1_det(sdet,std::string("DC_Container_det_") + std::to_string(region), region);

   Volume     DC_R1_vol("DC_R1_vol", R1_trap_sol2, air);
   DetElement DC_R1_det(sdet, std::string("DC_R1_det_") + std::to_string(region), region);

   DC_R1_vol.setVisAttributes( lcdd, x_det.visStr() );
   pv = DC_container_R1_vol.placeVolume( DC_R1_vol, width_diff );

   for(int sec = 1;  sec <= 6; sec++) {
     auto        r_r1   = clas12::geo::RegionRotation(sec,region);
     auto        trans_r1 = dd4hep::mm*clas12::geo::RegionTranslation(sec, region);
     Transform3D tr_R1( Rotation3D(
           r_r1.xx(), r_r1.xy(), r_r1.xz(),
           r_r1.yx(), r_r1.yy(), r_r1.yz(),
           r_r1.zx(), r_r1.zy(), r_r1.zz() ), Position(trans_r1.x(),trans_r1.y(),trans_r1.z()) );
     pv = assembly.placeVolume( DC_container_R1_vol, tr_R1 );
   }

   region = 2;
   Position width_diff2 = {
     0.0,
     1.0*clas12::geo::ContainerExtraShift(region)*dd4hep::mm,
     (clas12::geo::ContainerTrapWidth(region)*dd4hep::mm-clas12::geo::RegionTrapWidth(region)*dd4hep::mm)
   };

   Volume     DC_container_R2_vol("DC_container_R2_vol", R2_sol2, air);
   DetElement DC_container_R2_det(sdet,std::string("DC_Container_det_") + std::to_string(region), region);

   Volume     DC_R2_vol("DC_R2_vol", R2_trap_sol2, air);
   DetElement DC_R2_det(sdet, std::string("DC_R2_det_") + std::to_string(region), region);

   DC_R2_vol.setVisAttributes( lcdd, x_det.visStr() );
   pv = DC_container_R2_vol.placeVolume( DC_R2_vol, width_diff2 );

   for(int sec = 1;  sec <= 6; sec++) {
     auto        r_R2   = clas12::geo::RegionRotation(sec,region);
     auto        trans_R2 = dd4hep::mm*clas12::geo::RegionTranslation(sec, region);
     Transform3D tr_R2( Rotation3D(
           r_R2.xx(), r_R2.xy(), r_R2.xz(),
           r_R2.yx(), r_R2.yy(), r_R2.yz(),
           r_R2.zx(), r_R2.zy(), r_R2.zz() ), Position(trans_R2.x(),trans_R2.y(),trans_R2.z()) );
     pv = assembly.placeVolume( DC_container_R2_vol, tr_R2 );
   }

   region = 3;
   Position width_diff3 = {
     0.0,
     1.0*clas12::geo::ContainerExtraShift(region)*dd4hep::mm,
     (clas12::geo::ContainerTrapWidth(region)*dd4hep::mm-clas12::geo::RegionTrapWidth(region)*dd4hep::mm)
   };

   Volume     DC_container_R3_vol("DC_container_R3_vol", R3_sol2, air);
   DetElement DC_container_R3_det(sdet,std::string("DC_Container_det_") + std::to_string(region), region);

   Volume     DC_R3_vol("DC_R3_vol", R3_trap_sol2, air);
   DetElement DC_R3_det(sdet, std::string("DC_R3_det_") + std::to_string(region), region);

   DC_R3_vol.setVisAttributes( lcdd, x_det.visStr() );
   pv = DC_container_R3_vol.placeVolume( DC_R3_vol, width_diff3 );

   for(int sec = 1;  sec <= 6; sec++) {
     auto        r_R3   = clas12::geo::RegionRotation(sec,region);
     auto        trans_R3 = dd4hep::mm*clas12::geo::RegionTranslation(sec, region);
     Transform3D tr_R3( Rotation3D(
           r_R3.xx(), r_R3.xy(), r_R3.xz(),
           r_R3.yx(), r_R3.yy(), r_R3.yz(),
           r_R3.zx(), r_R3.zy(), r_R3.zz() ), Position(trans_R3.x(),trans_R3.y(),trans_R3.z()) );
     pv = assembly.placeVolume( DC_container_R3_vol, tr_R3 );
   }

   //G4VPhysicalVolume * phys = new G4PVPlacement(
   //      G4Transform3D(
   //         RegionRotation(sec,region),
   //         RegionTranslation(sec, region)
   //      ),
   //      fRegionContainers_log[index],          // its logical volume
   //      Form("region%d_phys",region), // its name
   //      mother,                       // its mother (logical) volume
   //      false,                        // no boolean operations
   //      sec,                     // its copy number
   //temp_box    = new G4Box("clipping_DC_placement_box_solid",1*m,1*m,1*m);
   //temp_region    = new G4GenericTrap("clippingR1trap_solid",  clas12::geo::RegionTrapWidth(1), clas12::geo::RegionTrapPoints(1));
   //fClippingRegion1_solid = new G4IntersectionSolid("ClippingRI_solid", temp_box, temp_region, 0,  G4ThreeVector(0.0,0.0,clas12::geo::RegionTrapOffset(1)) );

   //// ---------------------------------------------
   //// Region 2
   //temp_region                = new G4GenericTrap("R2Containertrap_solid",  clas12::geo::ContainerTrapWidth(2), clas12::geo::ContainerTrapPoints(2));
   //fRegionContainers_solid[1] = new G4IntersectionSolid("R2Container_solid", temp_box, temp_region, 0,  G4ThreeVector(0.0,0.0,clas12::geo::ContainerTrapWidth(2)) );

   //temp_region    = new G4GenericTrap("R2trap_solid",  clas12::geo::RegionTrapWidth(2), clas12::geo::RegionTrapPoints(2));
   //fRegions_solid[1] = new G4IntersectionSolid("RI_solid", temp_box, temp_region, 0,  G4ThreeVector(0.0,0.0,clas12::geo::RegionTrapOffset(2)) );

   //// ---------------------------------------------
   //// Region 3
   //temp_region                = new G4GenericTrap("R3Containertrap_solid",  clas12::geo::ContainerTrapWidth(3), clas12::geo::ContainerTrapPoints(3));
   //fRegionContainers_solid[2] = new G4IntersectionSolid("R3Container_solid", temp_box, temp_region, 0,  G4ThreeVector(0.0,0.0,clas12::geo::ContainerTrapWidth(3)) );

   //temp_region    = new G4GenericTrap("R3trap_solid",  clas12::geo::RegionTrapWidth(3), clas12::geo::RegionTrapPoints(3));
   //fRegions_solid[2] = new G4IntersectionSolid("RI_solid", temp_box, temp_region, 0,  G4ThreeVector(0.0,0.0,clas12::geo::RegionTrapOffset(3)) );


   // ---------------------------------------------
   //
   int zplanes = 14; // number of planes in z directions
   std::vector<double> rInner = {
      536.25*mm, 390*mm, 390*mm, 390*mm, 390*mm,
      390*mm, 390*mm, 390*mm, 390*mm, 390*mm,
      390*mm, 390*mm, 390*mm, 784*mm
   };
   std::vector<double> rOuter = { 
      1020*mm, 1020*mm, 1038*mm, 1038*mm, 1020*mm,
      1020*mm, 1038*mm, 1038*mm, 1020*mm, 1020*mm,
      1038*mm, 1038*mm, 1020*mm, 1020*mm
   };
   std::vector<double> zPlane = { 
      0*mm, 425*mm, 440.7*mm, 546.4*mm, 562.1*mm,
      814.92*mm, 830.62*mm, 936.32*mm, 952.02*mm, 1204.84*mm,
      1220.54*mm, 1326.24*mm, 1341.94*mm, 1783.44*mm
   };

   Polycone solenoid( 0,  ///< Initial Phi starting angle
         360*deg,  ///< Total Phi angle
         rInner,        ///< Tangent distance to inner surface
         rOuter,        ///< Tangent distance to outer surface
         zPlane);       ///< z coordinate of corners
   Position fSolenoid_pos = {0*cm,0.0*cm,-898.093*mm};
  Volume solenoid_vol("solenoid_vol", solenoid, air);
  solenoid_vol.setVisAttributes(lcdd, "GrayVis");
  pv = assembly.placeVolume(solenoid_vol, fSolenoid_pos);



  // --------------------------------------------------
  // HTCC
  std::vector<double> rInner_htcc = { 0*mm, 15.8*mm, 91.5*mm, 150*mm };
  std::vector<double> rOuter_htcc = { 1742*mm, 2300*mm, 2300*mm, 1289*mm };
  std::vector<double> zPlane_htcc = { -275*mm, 181*mm, 1046*mm, 1740*mm };

  Polycone htccBigGasVolume_solid(
         0,        ///< Initial Phi starting angle
         360*deg,  ///< Total Phi angle
         rInner_htcc,   ///< Tangent distance to inner surface
         rOuter_htcc,   ///< Tangent distance to outer surface
         zPlane_htcc);  ///< z coordinate of corners

   //______________________________________________________________________________
   std::vector<double> rInner_2 = { 0*mm, 0*mm, 0*mm, 0*mm, 0*mm, 0*mm, 0*mm, 0*mm, 0*mm, 0*mm,
      0*mm, 0*mm, 0*mm, 0*mm, 0*mm, 0*mm, 0*mm};
   std::vector<double>  rOuter_2 = {1416.05*mm, 1410.081*mm, 1404.493*mm, 1398.905*mm, 1393.317*mm,
      1387.729*mm, 1387.729*mm, 1363.4466*mm, 1339.1388*mm, 1305.8648*mm,
      1272.5908*mm, 1239.3168*mm, 1206.0174*mm, 1158.24*mm, 1105.408*mm,
      996.0356*mm, 945.896*mm};
   std::vector<double>  zPlane_2 = {-276.4122*mm, -178.6222*mm, -87.1822*mm, 4.2578*mm, 95.6978*mm,
      187.1378*mm, 278.5778*mm, 370.0178*mm, 461.4578*mm, 552.8978*mm,
      644.3378*mm, 735.7778*mm, 827.2178*mm, 918.6578*mm, 991.378*mm,
      1080.278*mm, 1107.71*mm };
   Polycone htccEntryDishVolume_solid( 
         0,         ///< Initial Phi starting angle
         360*deg,   ///< Total Phi angle
         rInner_2,         ///< Tangent distance to inner surface
         rOuter_2,        ///< Tangent distance to outer surface
         zPlane_2);         ///< z coordinate of corners

   //______________________________________________________________________________
   std::vector<double> rInner_3 = {0*mm, 0*mm, 0*mm,
      0*mm, 0*mm, 0*mm,
      0*mm, 0*mm, 0*mm};
   std::vector<double> rOuter_3 = { 257.505*mm, 323.952*mm, 390.373*mm,
      456.819*mm, 525.831*mm, 599.872*mm,
      673.913*mm, 747.979*mm, 827.151*mm};

   std::vector<double> zPlane_3 = {
      380*mm, 470.17*mm, 561.61*mm,
      653.05*mm, 744.49*mm, 835.93*mm,
      927.37*mm, 1018.81*mm, 1116.6*mm};

   Polycone htccEntryConeVolume_solid(
       0,         ///< Initial Phi starting angle
         360*deg,   ///< Total Phi angle
         rInner_3,         ///< Tangent distance to inner surface
         rOuter_3,        ///< Tangent distance to outer surface
         zPlane_3);         ///< z coordinate of corners


   SubtractionSolid htccEntryDishCone_solid(htccEntryDishVolume_solid , htccEntryConeVolume_solid);

   SubtractionSolid htcc_solid(htccBigGasVolume_solid , htccEntryDishCone_solid);

   //   htcc_phys = new G4PVPlacement(
   //         0,
   //         G4ThreeVector(0,0,0),
   //         htcc_log,//htcc_log,          // its logical volume
   //         "htcc_phys", // its name
   //         mother,                       // its mother (logical) volume
   //         false,                        // no boolean operations
   //         0,                     // its copy number
   //         true);                        // check for overlaps
   //}

  Volume htcc_vol("htcc_vol", htcc_solid, air);
  htcc_vol.setVisAttributes(lcdd, "BlueVis");
  pv = assembly.placeVolume(htcc_vol);

   //______________________________________________________________________________

   pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
   pv.addPhysVolID("system",sdet.id()).addPhysVolID("barrel",0);
   sdet.setPlacement(pv);
   return sdet;



  // Cryostat
  //double r_inner = 160.0*dd4hep::cm;
  //double r_outer = 240.0*dd4hep::cm;
  //double z_inner = 200.0*dd4hep::cm;
  //double z_outer = 450.0*dd4hep::cm;
  //double z_offset = 0.0;

  //DetElement layer(sdet,"Cryostat",1);
  //Material mat = air;
  //string cryostat_name= "cryostat";
  //Polycone  crystotat_solid(0.0, 2.0*TMath::Pi(),
  //    {r_outer, r_inner, r_inner, r_outer},
  //    {r_outer, r_outer, r_outer,  r_outer},
  //    {-1.0*z_outer, -1.0*z_inner, 1.0*z_inner, 1.0*z_outer});
  //Volume s_vol(cryostat_name, crystotat_solid, mat);
  //s_vol.setVisAttributes(lcdd,x_det.visStr());
  ////pv = assembly.placeVolume(s_vol,Position(0, 0, z_offset));
  //

  //DetElement beampipe_det(sdet,"BeamPipe_det",2);
  //string beampipe_name= "beampipe";
  //Polycone  beampipe_solid(0.0, 2.0*TMath::Pi(),
  //    {0.0*dd4hep::cm, 0.0*dd4hep::cm},
  //    {1.0*dd4hep::cm, 1.0*dd4hep::cm},
  //    {-800*dd4hep::cm,  800*dd4hep::cm});
  //Volume beampipe_vol(beampipe_name, beampipe_solid, mat);
  //beampipe_vol.setVisAttributes(lcdd,"GrayVis");
  //pv = assembly.placeVolume(beampipe_vol,Position(0, 0, 0));

  //DetElement forward_hcal_det(sdet,"forward_hcal_det",2);
  //string forward_hcal_name= "forward_hcal";
  //Polycone  forward_hcal_solid(0.0, 2.0*TMath::Pi(),
  //    {5.0*dd4hep::cm, 5.0*dd4hep::cm},
  //    {50.0*dd4hep::cm, 50.0*dd4hep::cm},
  //    {-40*dd4hep::cm,  40*dd4hep::cm});
  //Volume forward_hcal_vol(forward_hcal_name, forward_hcal_solid, mat);
  //forward_hcal_vol.setVisAttributes(lcdd,"PurpleVis");
  //pv = assembly.placeVolume(forward_hcal_vol,Position(0, 0, -600*dd4hep::cm));

  //DetElement ion_gem_trackers_det(sdet,"ion_gem_trackers_det",2);
  //string ion_gem_trackers_name= "forward_hcal";
  //Polycone  ion_gem_trackers_solid(0.0, 2.0*TMath::Pi(),
  //    {5.0*dd4hep::cm, 5.0*dd4hep::cm},
  //    {43.0*dd4hep::cm, 35.0*dd4hep::cm},
  //    {-38*dd4hep::cm,  38*dd4hep::cm});
  //Volume ion_gem_trackers_vol(ion_gem_trackers_name, ion_gem_trackers_solid, mat);
  //ion_gem_trackers_vol.setVisAttributes(lcdd,"GreenVis");
  //pv = assembly.placeVolume(ion_gem_trackers_vol,Position(0, 0, -450*dd4hep::cm));

  //DetElement emcal_sphere_det(sdet,"emcal_sphere_det",2);
  //string emcal_sphere_name= "forward_hcal";
  //Sphere  emcal_sphere_solid(310*dd4hep::cm, 335*dd4hep::cm,
  //      TMath::Pi()/15., TMath::Pi()/5,
  //      0, 2*TMath::Pi());  
  //Volume emcal_sphere_vol(emcal_sphere_name, emcal_sphere_solid, mat);
  //emcal_sphere_vol.setVisAttributes(lcdd,"RBG015");
  //pv = assembly.placeVolume(emcal_sphere_vol,Position(0, 0, 0*dd4hep::cm));

  //DetElement tof_sphere_det(sdet,"tof_sphere_det",2);
  //string tof_sphere_name= "forward_hcal";
  //Sphere  tof_sphere_solid(300*dd4hep::cm, 305*dd4hep::cm,
  //      TMath::Pi()/15., TMath::Pi()/4.75,
  //      0, 2*TMath::Pi());  
  //Volume tof_sphere_vol(tof_sphere_name, tof_sphere_solid, mat);
  //tof_sphere_vol.setVisAttributes(lcdd,"RedVis");
  //pv = assembly.placeVolume(tof_sphere_vol,Position(0, 0, 0*dd4hep::cm));

  //DetElement RICH_sphere_det(sdet,"RICH_sphere_det",2);
  //string RICH_sphere_name= "forward_hcal";
  //Sphere  RICH_sphere_solid(285*dd4hep::cm, 295*dd4hep::cm,
  //      TMath::Pi()/15., TMath::Pi()/4.5,
  //      0, 2*TMath::Pi());  
  //Volume RICH_sphere_vol(RICH_sphere_name, RICH_sphere_solid, mat);
  //RICH_sphere_vol.setVisAttributes(lcdd,"OrangeVis");
  //pv = assembly.placeVolume(RICH_sphere_vol,Position(0, 0, 0*dd4hep::cm));




  //for(xml_coll_t i(x_det,_U(layer)); i; ++i, ++n)  {
  //  xml_comp_t x_layer = i;
  //  string  l_name = det_name+_toString(n,"_layer%d");
  //  double  z    = x_layer.outer_z();
  //  double  rmin = x_layer.inner_r();
  //  double  z_offset = 0.0;
  //  if(x_layer.hasAttr(_Unicode(z_offset))) {
  //    z_offset = x_layer.z_offset();
  //  }
  //  double  r    = rmin;
  //  DetElement layer(sdet,_toString(n,"layer%d"),x_layer.id());
  //  int m = 0;

  //  printout(INFO, "GenericShapeJLEIC", "Creating a Generic Layer");
  //  for(xml_coll_t j(x_layer,_U(slice)); j; ++j, ++m)  {
  //    xml_comp_t x_slice = j;
  //    Material mat = lcdd.material(x_slice.materialStr());
  //    string s_name= l_name+_toString(m,"_slice%d");
  //    double thickness = x_slice.thickness();
  //    Tube   s_tub(r, r+thickness,z);//,2.0*TMath::Pi()+0.01);
  //    Volume s_vol(s_name, s_tub, mat);

  //    r += thickness;
  //    s_vol.setVisAttributes(lcdd,x_det.visStr());
  //    pv = assembly.placeVolume(s_vol,Position(0, 0, z_offset));
  //    // Slices have no extra id. Take the ID of the layer!
  //    pv.addPhysVolID("slice",m);
  //    printout(INFO, "GenericShapeJLEIC", "Creating Generic Slice");
  //  }
  //  //cout << l_name << " " << rmin << " " << r << " " << z << endl;
  //    
  //}

  //pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
  //pv.addPhysVolID("system",sdet.id()).addPhysVolID("barrel",0);
  //sdet.setPlacement(pv);
  //return sdet;
}

DECLARE_DETELEMENT(StandInGeometryCLAS12,create_detector)
