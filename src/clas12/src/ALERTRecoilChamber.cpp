#include "ALERTRecoilChamber.h"
#include "DD4hep/DetFactoryHelper.h"

using namespace DD4hep;
using namespace DD4hep::Geometry;
  using namespace dd4hep;

ALERTRecoilChamber::ALERTRecoilChamber()
{
      //--Parameters of the gas detector
      innerRadiusOfTheGasDetector   = 30.00*dd4hep::mm;
      outerRadiusOfTheGasDetector   = 79.995*dd4hep::mm;
      hightOfTheGasDetector         = 200.*dd4hep::mm;
      startAngleOfTheGasDetector    = 0.*deg;
      spanningAngleOfTheGasDetector = 360.*deg;
       gasDetector_posX             = 0.*dd4hep::mm;
       gasDetector_posY             = 0.*dd4hep::mm;
       gasDetector_posZ             = 0.*dd4hep::mm;

      //--Parameters of the wires
      innerRadiusOfTheWire   = 0.00*dd4hep::mm;
      outerRadiusOfTheWire   = 0.04*dd4hep::mm;
      lengthOfTheWire        = 30.*dd4hep::cm;   // not the "hight"
      startAngleOfTheWire    = 0.*deg;
      spanningAngleOfTheWire = 360.*deg; 
      DeltaP                 = 2.0*dd4hep::mm; // *desired* wire separation around the circumference. 
}
//__________________________________________________________________________

ALERTRecoilChamber::~ALERTRecoilChamber()
{
}
    //__________________________________________________________________________


DD4hep::Geometry::BooleanSolid* ALERTRecoilChamber::BuildWireSolid(int layer, int subcell)
{
  ROOT::Math::RotationZ aROT(5.0*deg);
  std::string name = "trap" + std::to_string(layer) + "_sub" + std::to_string(subcell);
  auto R1_points = fRCGeometry.GetSubCellTrapPoints(layer,subcell);
  std::vector<double> R1_points2;
  std::for_each(R1_points.begin(), R1_points.end(), [&](const auto& twovec){
      R1_points2.push_back( twovec.x()*dd4hep::mm );
      R1_points2.push_back( twovec.y()*dd4hep::mm );
      });

  EightPointSolid trap(fRCGeometry.WireLength*dd4hep::mm/2.0, R1_points2.data());
  Box box(4.0*dd4hep::mm, 4.0*dd4hep::mm, fRCGeometry.WireLength*dd4hep::mm/2.0);

  auto tr = fRCGeometry.GetFirstCellTransform(layer,subcell);
  return new IntersectionSolid( box,trap, ROOT::Math::Transform3D(
        tr.xx(), tr.xy(), tr.xz(), tr.dx()*dd4hep::mm,
        tr.yx(), tr.yy(), tr.yz(), tr.dy()*dd4hep::mm,
        tr.zx(), tr.zy(), tr.zz(), tr.dz()*dd4hep::mm
        ));
}
//______________________________________________________________________________

void ALERTRecoilChamber::BuildUnitCells()
{
  DD4hep::Geometry::LCDD& lcdd = DD4hep::Geometry::LCDD::getInstance();
  Material   air       = lcdd.air();
  for(int lay = 0; lay<fRCGeometry.NLayers; lay++){
    //  _______ 
    //  \__|__/
    //   \_|_/

    fWireVolume_solid[lay][0] = BuildWireSolid(lay,0);
    fWireVolume_solid[lay][1] = BuildWireSolid(lay,1);
    fWireVolume_solid[lay][2] = BuildWireSolid(lay,2);
    fWireVolume_solid[lay][3] = BuildWireSolid(lay,3);

    //--Step max
    double maxStep = 1.0*mm;
    //G4UserLimits * fStepLimit = new G4UserLimits(maxStep);
    //fStepLimit->SetMaxAllowedStep(maxStep);
    //fStepLimit->SetUserMaxTrackLength(maxStep);

    for(int i = 0; i<4; i++){

      std::string log_name = "wire_volume_log_" 
        + std::to_string(lay) + "_part" + std::to_string(i);

      fWireVolume_log[lay][i] = Volume(
          log_name,
          *(fWireVolume_solid[lay][i]), 
          air );

      //fWireVolume_log[lay][i]->SetUserLimits(fStepLimit);
    }
  }
}
    //__________________________________________________________________________

void  ALERTRecoilChamber::PlaceCells(DD4hep::Geometry::Volume& mother, int layer, double z_rotation, int wire_number ){

   bool check_overlaps = false;
   for(int i = 0; i<4; i++) {
      std::string wire_name = "wire_volume_" + std::to_string(wire_number) + "_part" + std::to_string(i);
      //fWireVolume_log[layer][i]->SetSensitiveDetector(fSensitiveDetector);
      //fWireVolume_log[layer][i]->SetVisAttributes(vs);//G4VisAttributes::GetInvisible());
      //fWireVolume_log[layer][i]->SetVisAttributes(G4VisAttributes::GetInvisible());
      auto rot  =  fRCGeometry.GetSenseWireRotation(wire_number);
      auto tran =  fRCGeometry.GetSenseWirePosition(wire_number);

      //VisAttr vis;
      //if(layer%2==0){
      //vis.setColor(1.0,0,0);
      //} else {
      //vis.setColor(0.0,1.0,0);
      //}
      //vis.setAlpha(0.5);

      ROOT::Math::Transform3D tr(
          ROOT::Math::Rotation3D(
            rot.xx(), rot.xy(), rot.xz(),
            rot.yx(), rot.yy(), rot.yz(),
            rot.zx(), rot.zy(), rot.zz()
            ),
          ROOT::Math::Translation3D(tran.x()*dd4hep::mm, tran.y()*dd4hep::mm, tran.z()*dd4hep::mm));

      Volume aVol(fWireVolume_log[layer][i]);
      aVol.setVisAttributes(mother.visAttributes());
      PlacedVolume pv = mother.placeVolume(aVol, tr);


      //new G4PVPlacement(
      //      //   0,G4ThreeVector(0,0,0),
      //      G4Transform3D(
      //         fRCGeometry.GetSenseWireRotation(wire_number),
      //         fRCGeometry.GetSenseWirePosition(wire_number)),
      //      //G4Transform3D(*(fWireVolume_rot[layer][i]),G4ThreeVector(0,0,0.)),
      //      fWireVolume_log[layer][i], wire_name,
      //      mother,
      //      false,
      //      wire_number,
      //      check_overlaps);
   }
}

void  ALERTRecoilChamber::BuildDetector(DD4hep::Geometry::Volume& mother)
{
  int wire_number = 0;

  for(int lay=0; lay<fRCGeometry.NLayers; lay++){

    double PhiWire = fRCGeometry.fCellDeltaPhi.at(lay);

    for(int wi=0;wi<fRCGeometry.fNCells.at(lay); wi++){  

      PlaceCells( mother, lay, double(wi)*PhiWire, wire_number );

      wire_number++;
    }
  }   
}

