clas12
======

![clas12 detector](compact/clas12.png)

Using `compact/run_example` :

```bash
cd compact
./run_example
```

This runs geant4 using `ddsim` with the general particle source configured in 
`compact/gps.mac`. The compact file `clas12.xml` is used  as the geometry.
The script must be executed from the `compact` directory.


