#ifndef ALERTRecoilChamber_HH
#define ALERTRecoilChamber_HH

#include "DD4hep/DetFactoryHelper.h"
#include "DCWire.h"
#include "RCGeometry.h"
#include <array>

class ALERTRecoilChamber {
   protected:


   public:
      ALERTRecoilChamber();
      ~ALERTRecoilChamber();

      std::array<std::array<DD4hep::Geometry::BooleanSolid*, 4 >, 8> fWireVolume_solid;
      std::array<std::array<DD4hep::Geometry::Volume,  4 >, 8> fWireVolume_log;
      //std::array<std::array<VPhysicalVolume*,4 >, 8> fWireVolume_phys;
      //std::array<std::array<RotationMatrix* ,4 >, 8> fWireVolume_rot;

      clas12::geo::RCGeometry          fRCGeometry;

      //--Parameters of the gas detector
      double  innerRadiusOfTheGasDetector   ;//= 30.00*mm;
      double  outerRadiusOfTheGasDetector   ;//= 79.995*mm;
      double  hightOfTheGasDetector         ;//= 200.*mm;
      double  startAngleOfTheGasDetector    ;//= 0.*deg;
      double  spanningAngleOfTheGasDetector ;//= 360.*deg;
      double   gasDetector_posX             ;//= 0.*mm;
      double   gasDetector_posY             ;//= 0.*mm;
      double   gasDetector_posZ             ;//= 0.*mm;

      //--Parameters of the wires
      double  innerRadiusOfTheWire   ;//= 0.00*mm;
      double  outerRadiusOfTheWire   ;//= 0.04*mm;
      double  lengthOfTheWire        ;//= 30.*cm;   // not the "hight"
      double  startAngleOfTheWire    ;//= 0.*deg;
      double  spanningAngleOfTheWire ;//= 360.*deg; 
      double  DeltaP                 ;//= 2.0*mm; // *desired* wire separation around the circumference. 

      DD4hep::Geometry::BooleanSolid* BuildWireSolid(int layer, int subcell);
      void  BuildUnitCells();
      void  PlaceCells(DD4hep::Geometry::Volume& mother, int layer, double z_rotation, int wire_number );
      void  BuildDetector(DD4hep::Geometry::Volume& mother);



};
#endif
