//==========================================================================
//
// BeamPipe
//
// See compact/beam_pipe.xml for usage.
//
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "TMath.h"
#include <string>

using namespace std;
using namespace DD4hep;
using namespace DD4hep::Geometry;
using std::string;

static Ref_t create_detector(LCDD& lcdd, xml_h e, SensitiveDetector sens)
{
  xml_det_t  x_det     = e;
  string     det_name  = x_det.nameStr();
  Material   vacuum    = lcdd.vacuum();

  DetElement sdet( det_name, x_det.id() );
  Assembly   assembly( det_name+"_assembly");
  //auto components = e.children("component");

  PlacedVolume pv;
  int n = 0;

  for(xml_coll_t i_comp(x_det,"component"); i_comp; ++i_comp, ++n)  {

    string   l_name    = det_name + "_layer" + std::to_string(n);
    double   length    = i_comp.attr<double>( _Unicode(length) )/2.0;
    double   radius    = i_comp.attr<double>( _Unicode(radius) );
    double   thickness = i_comp.attr<double>( _Unicode(thickness) );
    double   z_offset  = i_comp.attr<double>( _Unicode(z) );
    Material mat       = lcdd.material(i_comp.attr<std::string>( _Unicode(material)));
    //if(x_layer.hasAttr(_Unicode(z_offset))) {
    //  z_offset = x_layer.z_offset();
    //}
    DetElement layer(sdet, std::string("layer") + std::to_string(n),n );
    int m = 0;

    printout(INFO, "BeamPipe", "Creating a Generic Layer");
    //for(xml_coll_t j(x_layer,_U(slice)); j; ++j, ++m)  {
    //xml_comp_t x_slice = j;
    string s_name= l_name+_toString(m,"_slice%d");
    //double thickness = x_slice.thickness();
    Tube   s_tub(radius, radius+thickness, length);
    Volume s_vol(s_name, s_tub, mat);

    //r += thickness;
    s_vol.setVisAttributes(lcdd,x_det.visStr());
    pv = assembly.placeVolume(s_vol,Position(0, 0, z_offset));
    // Slices have no extra id. Take the ID of the layer!
    pv.addPhysVolID("slice",m);
    printout(INFO, "BeamPipe", "Creating Generic Slice");
      
  }

  pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
  pv.addPhysVolID("system",sdet.id()).addPhysVolID("barrel",0);
  sdet.setPlacement(pv);
  return sdet;
}

DECLARE_DETELEMENT(BeamPipe,create_detector)

