#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "TMath.h"
#include "DDRec/Surface.h"
#include "DDRec/DetectorData.h"

using namespace std;
using namespace DD4hep;
using namespace DD4hep::Geometry;
using namespace DD4hep::DDRec;
using namespace DDSurfaces;

static Ref_t create_detector(LCDD& lcdd, xml_h e, SensitiveDetector sens)
{
  typedef vector<PlacedVolume> Placements;

  xml_det_t   x_det     = e;
  Material    air       = lcdd.air();
  Material    carbon    = lcdd.material("CarbonFiber");
  Material    silicon   = lcdd.material("SiliconOxide");
  int         det_id    = x_det.id();
  string      det_name  = x_det.nameStr();
  PlacedVolume             pv;

  double      inner_r  = x_det.attr<double>(  _Unicode(inner_r) ) ;
  DetElement  sdet(det_name, det_id);
  Assembly    assembly(det_name+"_assembly");
  //printout(INFO, "SiTrackerBarrelJLEIC", "In the Create Detector Function");

  sens.setType("tracker");
  string module_name = "VtxBarrelModule";

  double  init_layer_radius = inner_r;//380.0*dd4hep::mm;

  // Critical parameters
  int                  N_layers      = 3; // Total number of layers
  std::vector<int>     N_z_modules   = { 1,    1,    1  };//, 1     }; // Number of modules in Z
  std::vector<int>     N_phi_modules = { 8,   14,   20  };//, 20 }; // Number of modules in Z

  double extra_module_width = 0.2*dd4hep::cm;
  double module_x =  (2.0*TMath::Pi()*init_layer_radius)/(N_phi_modules.at(0)*2.0);
  double module_y = 100.0*dd4hep::cm/2.0;
  double module_z =   2.0*dd4hep::mm;
  
  Position offset{ 0.0, 0.0, 1.0*dd4hep::m };

  // For each layer, calculate the delta_phi covered by each module 
  std::vector<double>  delta_phi_layer;
  std::for_each(N_phi_modules.begin(), N_phi_modules.end(),
      [&](auto const& n){ delta_phi_layer.push_back( TMath::Pi()/double(n) ); });

  // Calculate the radius for each layer
  std::vector<double>  layer_radius;
  std::for_each(delta_phi_layer.begin(), delta_phi_layer.end(),
      [&](auto const& p){ layer_radius.push_back( module_x/p ); });

  Volume module_vol(module_name, Box(module_x+extra_module_width, module_y-1.0*dd4hep::cm, module_z), carbon);
  Volume sense_vol("sense_volume", Box(module_x+extra_module_width-1.0*dd4hep::mm, module_y-2.0*dd4hep::cm, 0.5*dd4hep::mm), silicon);

  PlacedVolume sensitive_pv = module_vol.placeVolume( sense_vol );
  sensitive_pv.addPhysVolID( "sensor", 1 );

  // -------- create a measurement plane for the tracking surface attched to the sensitive volume -----
  Vector3D u( 1. , 0. , 0. ) ;
  Vector3D v( 0. , 1. , 0. ) ;
  Vector3D n( 0. , 0. , 1. ) ;
  Vector3D o( 0. , 0. , 0. ) ;

  // compute the inner and outer thicknesses that need to be assigned to the tracking surface
  // depending on wether the support is above or below the sensor
  // The tracking surface is used in reconstruction. It provides  material thickness
  // and radation lengths needed for various algorithms, routines, etc.
  double inner_thickness = module_z/2.0;
  double outer_thickness = module_z/2.0;

  SurfaceType type( SurfaceType::Sensitive ) ;
  VolPlane    surf( sense_vol, type, inner_thickness , outer_thickness , u,v,n,o ) ;

  //--------------------------------------------

  sens.setType("tracker");
  sense_vol.setSensitiveDetector(sens);

  ROOT::Math::RotationX  Rx( TMath::Pi()/2.0 );
  ROOT::Math::RotationZ  Rz( TMath::Pi()/2.0 );

  for(int i_layer=0; i_layer<N_layers; i_layer++){

    int     layer_id   = i_layer + 1;
    string  layer_name = std::string("layer") + std::to_string(layer_id) ;
    double  phi0       = 0;
    double  phi_tilt   = 180.0*dd4hep::degree/(double(N_phi_modules.at(i_layer))+3.0);

    std::cout << "Radius of layer " << layer_id << " is " << layer_radius.at(i_layer) << endl;
    DetElement layer_DE( sdet, _toString(layer_id,"layer%d"), layer_id );
    Assembly   layer_assembly( layer_name+"_assembly" );
    pv = assembly.placeVolume( layer_assembly );
    pv.addPhysVolID( "layer", layer_id );
    layer_DE.setPlacement(pv);
    layer_DE.setAttributes(lcdd, layer_assembly, "", "", "SiVertexLayerVis");

    // Loop over the number of modules in phi.
    for (int i_phi = 0; i_phi < N_phi_modules.at(i_layer); i_phi++){

      double phi = phi0 + i_phi*2.0*delta_phi_layer.at(i_layer);

      // The radial displacement of the module
      ROOT::Math::Polar3DVector rho_module( layer_radius.at(i_layer), TMath::Pi()/2.0, phi );

      double z0 = -1.0*module_y*double(N_z_modules.at(i_layer)) + module_y;
      double dz =  2.0*module_y;
      
      // Loop over the number of modules in z.
      for (int i_Z = 0; i_Z < N_z_modules.at(i_layer); i_Z++){

        ROOT::Math::XYZVector z_displacement(0.0, 0.0, z0 + double(i_Z)*dz);

        int    i_module      = 1 + i_Z + i_phi*N_z_modules.at(i_layer);
        string a_module_name = _toString(i_module, "module%d");

        Transform3D tr( RotationZ(phi_tilt)*RotationZ(phi)*Rx, Rz*rho_module + z_displacement);

        DetElement module_DE(layer_DE, a_module_name, i_module);
        module_vol.setVisAttributes(lcdd, x_det.visStr());
        pv = layer_assembly.placeVolume( module_vol, tr );
        pv.addPhysVolID( "module", i_module );
        module_DE.setPlacement(pv);

        DetElement module_sense_DE( module_DE, std::string("sense_DE")+std::to_string(i_module), i_module );
        module_sense_DE.setPlacement( sensitive_pv );

      }
    }
  }

  sdet.setAttributes(lcdd, assembly,x_det.regionStr(),x_det.limitsStr(),x_det.visStr());
  assembly.setVisAttributes(lcdd.invisible());

  pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
  pv.addPhysVolID("system", det_id);      // Set the subdetector system ID.
  pv.addPhysVolID("barrel", 1);           // Flag this as a barrel subdetector.
  sdet.setPlacement(pv);

  assembly->GetShape()->ComputeBBox() ;

  return sdet;
}

DECLARE_DETELEMENT(SiliconTrackerBarrel, create_detector)
