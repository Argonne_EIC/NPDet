//==========================================================================
//
// Generic Tube Creator. Will not support sensitive segments.
//
// Control via .xml file as follows:
//      <lcdd> <detectors>
// #Must contain a detector child element,
//      <detector name ="DetName" type="GenericShape" >
// #Must define at least one layer,
//      <layer id="#(int)" inner_r="#(double)" outer_z="#(double)" >
// #Must define at least one slice,
//      <slice material="string" thickness="#(double)" >         
// #Close layer, detector,
//      </layer>
//      </detector>
//      </lcdd>
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "TMath.h"

using namespace std;
using namespace DD4hep;
using namespace DD4hep::Geometry;

static Ref_t create_detector(LCDD& lcdd, xml_h e, SensitiveDetector sens)  {
  xml_det_t  x_det     = e;
  string     det_name  = x_det.nameStr();
  Material   air       = lcdd.air();
  DetElement sdet        (det_name,x_det.id());
  Assembly   assembly    (det_name+"_assembly");

  PlacedVolume pv;

  int n = 0;

  std::cout << " TESTING ...\n";

  for(xml_coll_t i(x_det,_U(layer)); i; ++i, ++n)  {
    xml_comp_t x_layer = i;
    string  l_name = det_name+_toString(n,"_layer%d");
    double  z    = x_layer.outer_z();
    double  rmin = x_layer.inner_r();
    double  z_offset = 0.0;
    if(x_layer.hasAttr(_Unicode(z_offset))) {
      z_offset = x_layer.z_offset();
    }
    double  r    = rmin;
    DetElement layer(sdet,_toString(n,"layer%d"),x_layer.id());
    int m = 0;

    printout(INFO, "GenericShapeJLEIC", "Creating a Generic Layer");
    for(xml_coll_t j(x_layer,_U(slice)); j; ++j, ++m)  {
      xml_comp_t x_slice = j;
      Material mat = lcdd.material(x_slice.materialStr());
      string s_name= l_name+_toString(m,"_slice%d");
      double thickness = x_slice.thickness();
      Tube   s_tub(r, r+thickness,z);//,2.0*TMath::Pi()+0.01);
      Volume s_vol(s_name, s_tub, mat);

      r += thickness;
      s_vol.setVisAttributes(lcdd,x_det.visStr());
      pv = assembly.placeVolume(s_vol,Position(0, 0, z_offset));
      // Slices have no extra id. Take the ID of the layer!
      pv.addPhysVolID("slice",m);
      printout(INFO, "GenericShapeJLEIC", "Creating Generic Slice");
    }
    //cout << l_name << " " << rmin << " " << r << " " << z << endl;
      
  }

  pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
  pv.addPhysVolID("system",sdet.id()).addPhysVolID("barrel",0);
  sdet.setPlacement(pv);
  return sdet;
}

DECLARE_DETELEMENT(GenericShapeJLEIC,create_detector)
