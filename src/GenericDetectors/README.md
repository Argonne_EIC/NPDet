Generic Detectors Library
=========================

The goal of this library is to provide all the detectors needed to build up a 
full concept detector. To use the detectors a code snippet needs to be added to 
the compact files. Not everything is a *detector*  since objects like the "beam 
pipe" don't produce output hits. So in this sense this is a *generic detectors 
and apparatus library*.

List of detectors
-----------------

* BeamPipe_geo.cpp
* ForwardTrackerPlane.cpp
* SiliconTrackerBarrel.cpp
* EcalBarrel_geo.cpp

Preliminary Example
-------------------

This code snippet defines the "beam pipe",  which in the current implementation 
consists of 3 tubes.

```xml
<detectors>
  <detector name="BeamPipe" type="BeamPipe" vis="GrayVis">
    <component material="Aluminum" length="CentralBeamPipe_length" thickness="CentralBeamPipe_thickness" radius="CentralBeamPipe_radius" z="CentralBeamPipe_z"/>
    <component material="Aluminum" length="UpStreamBeamPipe_length" thickness="UpStreamBeamPipe_thickness" radius="UpStreamBeamPipe_radius" z="UpStreamBeamPipe_z"/>
    <component material="Aluminum" length="DownStreamBeamPipe_length" thickness="DownStreamBeamPipe_thickness" radius="DownStreamBeamPipe_radius" z="DownStreamBeamPipe_z"/>
  </detector>
</detectors>
```
![BeamPipe detector](doc/beam_pipe.png)




