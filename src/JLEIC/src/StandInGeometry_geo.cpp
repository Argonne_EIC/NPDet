//==========================================================================
//
// Generic Tube Creator. Will not support sensitive segments.
//
// Control via .xml file as follows:
//      <lcdd> <detectors>
// #Must contain a detector child element,
//      <detector name ="DetName" type="GenericShape" >
// #Must define at least one layer,
//      <layer id="#(int)" inner_r="#(double)" outer_z="#(double)" >
// #Must define at least one slice,
//      <slice material="string" thickness="#(double)" >         
// #Close layer, detector,
//      </layer>
//      </detector>
//      </lcdd>
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "TMath.h"

using namespace std;
using namespace DD4hep;
using namespace DD4hep::Geometry;

static Ref_t create_detector(LCDD& lcdd, xml_h e, SensitiveDetector sens)
{
  xml_det_t  x_det     = e;
  string     det_name  = x_det.nameStr();
  Material   air       = lcdd.air();
  DetElement sdet        (det_name,x_det.id());
  Assembly   assembly    ("cryostat_assembly");

  PlacedVolume pv;

  int n = 0;

  std::cout << " TESTING ...\n";


  // Cryostat
  double r_inner = 160.0*dd4hep::cm;
  double r_outer = 240.0*dd4hep::cm;
  double z_inner = 200.0*dd4hep::cm;
  double z_outer = 450.0*dd4hep::cm;
  double z_offset = 0.0;

  DetElement layer(sdet,"Cryostat",1);
  Material mat = air;
  string cryostat_name= "cryostat";
  Polycone  crystotat_solid(0.0, 2.0*TMath::Pi(),
      {r_outer, r_inner, r_inner, r_outer},
      {r_outer, r_outer, r_outer,  r_outer},
      {-1.0*z_outer, -1.0*z_inner, 1.0*z_inner, 1.0*z_outer});
  Volume s_vol(cryostat_name, crystotat_solid, mat);
  s_vol.setVisAttributes(lcdd,x_det.visStr());
  //pv = assembly.placeVolume(s_vol,Position(0, 0, z_offset));
  

  DetElement beampipe_det(sdet,"BeamPipe_det",2);
  string beampipe_name= "beampipe";
  Polycone  beampipe_solid(0.0, 2.0*TMath::Pi(),
      {0.0*dd4hep::cm, 0.0*dd4hep::cm},
      {1.0*dd4hep::cm, 1.0*dd4hep::cm},
      {-800*dd4hep::cm,  800*dd4hep::cm});
  Volume beampipe_vol(beampipe_name, beampipe_solid, mat);
  beampipe_vol.setVisAttributes(lcdd,"GrayVis");
  pv = assembly.placeVolume(beampipe_vol,Position(0, 0, 0));

  DetElement forward_hcal_det(sdet,"forward_hcal_det",2);
  string forward_hcal_name= "forward_hcal";
  Polycone  forward_hcal_solid(0.0, 2.0*TMath::Pi(),
      {5.0*dd4hep::cm, 5.0*dd4hep::cm},
      {50.0*dd4hep::cm, 50.0*dd4hep::cm},
      {-40*dd4hep::cm,  40*dd4hep::cm});
  Volume forward_hcal_vol(forward_hcal_name, forward_hcal_solid, mat);
  forward_hcal_vol.setVisAttributes(lcdd,"PurpleVis");
  pv = assembly.placeVolume(forward_hcal_vol,Position(0, 0, -600*dd4hep::cm));

  DetElement ion_gem_trackers_det(sdet,"ion_gem_trackers_det",2);
  string ion_gem_trackers_name= "forward_hcal";
  Polycone  ion_gem_trackers_solid(0.0, 2.0*TMath::Pi(),
      {5.0*dd4hep::cm, 5.0*dd4hep::cm},
      {43.0*dd4hep::cm, 35.0*dd4hep::cm},
      {-38*dd4hep::cm,  38*dd4hep::cm});
  Volume ion_gem_trackers_vol(ion_gem_trackers_name, ion_gem_trackers_solid, mat);
  ion_gem_trackers_vol.setVisAttributes(lcdd,"GreenVis");
  pv = assembly.placeVolume(ion_gem_trackers_vol,Position(0, 0, -450*dd4hep::cm));

  DetElement emcal_sphere_det(sdet,"emcal_sphere_det",2);
  string emcal_sphere_name= "forward_hcal";
  Sphere  emcal_sphere_solid(310*dd4hep::cm, 335*dd4hep::cm,
        TMath::Pi()/15., TMath::Pi()/5,
        0, 2*TMath::Pi());  
  Volume emcal_sphere_vol(emcal_sphere_name, emcal_sphere_solid, mat);
  emcal_sphere_vol.setVisAttributes(lcdd,"RBG015");
  pv = assembly.placeVolume(emcal_sphere_vol,Position(0, 0, 0*dd4hep::cm));

  DetElement tof_sphere_det(sdet,"tof_sphere_det",2);
  string tof_sphere_name= "forward_hcal";
  Sphere  tof_sphere_solid(300*dd4hep::cm, 305*dd4hep::cm,
        TMath::Pi()/15., TMath::Pi()/4.75,
        0, 2*TMath::Pi());  
  Volume tof_sphere_vol(tof_sphere_name, tof_sphere_solid, mat);
  tof_sphere_vol.setVisAttributes(lcdd,"RedVis");
  pv = assembly.placeVolume(tof_sphere_vol,Position(0, 0, 0*dd4hep::cm));

  DetElement RICH_sphere_det(sdet,"RICH_sphere_det",2);
  string RICH_sphere_name= "forward_hcal";
  Sphere  RICH_sphere_solid(285*dd4hep::cm, 295*dd4hep::cm,
        TMath::Pi()/15., TMath::Pi()/4.5,
        0, 2*TMath::Pi());  
  Volume RICH_sphere_vol(RICH_sphere_name, RICH_sphere_solid, mat);
  RICH_sphere_vol.setVisAttributes(lcdd,"OrangeVis");
  pv = assembly.placeVolume(RICH_sphere_vol,Position(0, 0, 0*dd4hep::cm));




  //for(xml_coll_t i(x_det,_U(layer)); i; ++i, ++n)  {
  //  xml_comp_t x_layer = i;
  //  string  l_name = det_name+_toString(n,"_layer%d");
  //  double  z    = x_layer.outer_z();
  //  double  rmin = x_layer.inner_r();
  //  double  z_offset = 0.0;
  //  if(x_layer.hasAttr(_Unicode(z_offset))) {
  //    z_offset = x_layer.z_offset();
  //  }
  //  double  r    = rmin;
  //  DetElement layer(sdet,_toString(n,"layer%d"),x_layer.id());
  //  int m = 0;

  //  printout(INFO, "GenericShapeJLEIC", "Creating a Generic Layer");
  //  for(xml_coll_t j(x_layer,_U(slice)); j; ++j, ++m)  {
  //    xml_comp_t x_slice = j;
  //    Material mat = lcdd.material(x_slice.materialStr());
  //    string s_name= l_name+_toString(m,"_slice%d");
  //    double thickness = x_slice.thickness();
  //    Tube   s_tub(r, r+thickness,z);//,2.0*TMath::Pi()+0.01);
  //    Volume s_vol(s_name, s_tub, mat);

  //    r += thickness;
  //    s_vol.setVisAttributes(lcdd,x_det.visStr());
  //    pv = assembly.placeVolume(s_vol,Position(0, 0, z_offset));
  //    // Slices have no extra id. Take the ID of the layer!
  //    pv.addPhysVolID("slice",m);
  //    printout(INFO, "GenericShapeJLEIC", "Creating Generic Slice");
  //  }
  //  //cout << l_name << " " << rmin << " " << r << " " << z << endl;
  //    
  //}

  pv = lcdd.pickMotherVolume(sdet).placeVolume(assembly);
  pv.addPhysVolID("system",sdet.id()).addPhysVolID("barrel",0);
  sdet.setPlacement(pv);
  return sdet;
}

DECLARE_DETELEMENT(StandInGeometryJLEIC,create_detector)
